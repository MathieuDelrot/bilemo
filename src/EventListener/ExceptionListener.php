<?php


namespace App\EventListener;


use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;


class ExceptionListener implements EventSubscriberInterface
{

    public function onKernelException(ExceptionEvent $event)
    {

        $exception = $event->getThrowable();

        if ($exception instanceof HttpException) {
            $status = $exception->getStatusCode();
        } else {
            $status = '500';
        }

        $data = [
            'status' => $status,
            'message' => $exception->getMessage()
        ];

        $response = new JsonResponse($data);

        $event->setResponse($response);
    }

    public static function getSubscribedEvents()
    {
        return [
            'kernel.exception' => 'onKernelException',
        ];
    }

}