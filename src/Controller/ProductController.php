<?php


namespace App\Controller;


use App\Entity\Product;
use App\Helper\Pagination;
use Doctrine\ORM\QueryBuilder;
use Psr\Cache\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;
use JMS\Serializer\SerializerInterface as JMSSerializer;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;


/**
 * Class ProductController
 * @package App\Controller
 * @Route("/api/products")
 */
class ProductController extends AbstractController
{

    private $serialier;

    /**
     * UserController constructor.
     * @param $serialier
     */
    public function __construct(JMSSerializer $serialier)
    {
        $this->serialier = $serialier;
    }


    /**
     * @Route(name="api_products_collection_get", methods={"GET"})
     * @param Request $request
     * @param CacheInterface $cache
     * @return Response
     * @throws InvalidArgumentException
     */
    public function collection(Request $request, CacheInterface $cache): Response
    {

        $page = $request->query->get('page', 1);

        $response = $cache->get('get-products-'.$page, function (ItemInterface $item) use($page) {

            $item->expiresAfter(86400);

            /** @var QueryBuilder $qb */
            $qb = $this->getDoctrine()->getRepository(Product::class)->findAllQueryBuilder();

            $pagination = new Pagination($page, $qb);


            $json = $this->serialier->serialize($pagination->getItems(), 'json');

            return new Response($json, Response::HTTP_OK,
                [
                    'Content-Type' => 'application/json',
                    'total' => $pagination->getTotal(),
                    'count' => $pagination->getCount(),
                ]);
        });

        return $response;
    }

    /**
     * @Route("/{id}", name="api_products_item_get", methods={"GET"})
     * @param int $id
     * @param CacheInterface $cache
     * @return Response
     * @throws InvalidArgumentException
     */
    public function item(int $id, CacheInterface $cache) : Response
    {

        return $cache->get('get-product-'.$id, function (ItemInterface $item) use($id) {

            $item->expiresAfter(86400);

            $product = $this->getDoctrine()->getRepository(Product::class)->find($id);

            if($product === null)
                throw new HttpException(Response::HTTP_NOT_FOUND, 'No product with this id');

            $json = $this->serialier->serialize($product, 'json');

            return new Response($json, Response::HTTP_OK, ['Content-Type' => 'application/json',]);
        });

    }

}