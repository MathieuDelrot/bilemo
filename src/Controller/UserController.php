<?php


namespace App\Controller;

use App\Entity\Customer;
use App\Entity\Product;
use App\Entity\User;
use App\Helper\Pagination;
use Doctrine\ORM\Mapping\Entity;
use Exception;
use JMS\Serializer\SerializationContext;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\SerializerInterface;
use JMS\Serializer\SerializerInterface as JMSSerializer;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;


/**
 * Class ProductController
 * @package App\Controller
 * @Route("/api/users")
 */
class UserController extends AbstractController
{

    private $serialier;

    /**
     * UserController constructor.
     * @param $serialier
     */
    public function __construct(JMSSerializer $serialier)
    {
        $this->serialier = $serialier;
    }


    /**
     * @Route(name="api_users_collection_get", methods={"GET"})
     * @param Request $request
     * @param CacheInterface $cache
     * @return Response
     * @throws InvalidArgumentException
     */
    public function collection(Request $request, CacheInterface $cache): Response
    {
        $page = $request->query->get('page', 1);

        $customerId = $request->query->get('customerId');

        $response = $cache->get('get-users-'.$page.'-'.$customerId, function (ItemInterface $item) use($page, $customerId) {

            $item->expiresAfter(86400);

            $qb = $this->getDoctrine()->getRepository(User::class)->findAllQueryBuilder($customerId);

            $pagination = New Pagination($page, $qb);

            $json = $this->serialier->serialize($pagination->getItems(), 'json', SerializationContext::create()->setGroups(['users']));

            return new Response($json, Response::HTTP_OK,
                [
                    'Content-Type' => 'application/json',
                    'total' => $pagination->getTotal(),
                    'count' => $pagination->getCount(),
                ]);
        });

        return $response;

    }

    /**
     * @Route("/{id}",name="api_users_item_get", methods={"GET"})
     * @param int $id
     * @param CacheInterface $cache
     * @return JsonResponse
     * @throws InvalidArgumentException
     */
    public function item(int $id, CacheInterface $cache): Response
    {

        return $cache->get('get-user-'.$id, function (ItemInterface $item) use($id) {
            $item->expiresAfter(86400);

            $user = $this->getDoctrine()->getRepository(User::class)->find($id);

            if($user === null)
                throw new HttpException(Response::HTTP_NOT_FOUND ,'No user with this id');

            $json = $this->serialier->serialize($user, 'json', SerializationContext::create()->setGroups(['users']));

            return new Response($json, Response::HTTP_OK, ['Content-Type' => 'application/json',]);
        });


    }

    /**
     * @Route(name="api_users_item_post", methods={"POST"})
     * @param Request $request
     * @param SerializerInterface $serializer
     * @param UrlGeneratorInterface $urlGenerator
     * @param Security $security
     * @return JsonResponse
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function post(Request $request, SerializerInterface $serializer, UrlGeneratorInterface $urlGenerator, Security $security, CacheInterface $cache, ValidatorInterface $validator): JsonResponse
    {
        /** @var User $user */
        $user = $serializer->deserialize($request->getContent(), User::class, 'json');

        $errors = $validator->validate($user);

        if (count($errors) > 0)
            throw new HttpException(Response::HTTP_UNPROCESSABLE_ENTITY,(string)$errors);

        $customer = $security->getUser();

        if ($customer instanceof Customer) {
            $user->setCustomer($customer);
        } else {
            throw new HttpException(Response::HTTP_FORBIDDEN, 'User logged is not type of Customer');
        }

        $em =  $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        $this->clearCacheAfterOperation($user, $cache);

        return new JsonResponse(
            $serializer->serialize($user, 'json', ['groups' => 'users']),
            Response::HTTP_CREATED,
        ['Location' => $urlGenerator->generate('api_users_collection_get', ['id' => $user->getId()])], true);
    }

    /**
     * @Route("/{id}",name="api_users_item_delete", methods={"DELETE"})
     * @param int $id
     * @return JsonResponse
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function delete(int $id, CacheInterface $cache): JsonResponse
    {
        /** @var User $user */
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);

        if($user === null)
            throw new HttpException( Response::HTTP_NOT_FOUND, 'No user with this id');

        if($user) {
           $this->getDoctrine()->getManager()->remove($user);
        }

        $this->clearCacheAfterOperation($user, $cache);

        $this->getDoctrine()->getManager()->flush();

        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @param User $user
     * @param CacheInterface $cache
     * @throws InvalidArgumentException
     */
    private function clearCacheAfterOperation(User $user, CacheInterface $cache)
    {

        $usersIds =  $this->getDoctrine()->getRepository(User::class)->findAllIds();

        $page = Pagination::getPageNumberForSpecificItem($user->getId(), $usersIds);
        $pageQuantity = Pagination::getPageQuantity($usersIds);

        if($page === null)
            throw new HttpException( Response::HTTP_PARTIAL_CONTENT, 'No page found with this id');

        if ($user instanceof User) {
            $customerId = $user->getCustomer()->getId();

            if($page) {
                $cache->delete('get-user-'.$user->getId());
                $cache->delete('get-users-'.$page.'-'.$customerId);
                if ($pageQuantity) {
                    $i = 1;
                    while ($i <= $pageQuantity) {
                        $cache->delete('get-users-'.$i.'-');
                        $i++;
                    }
                }
            }
        }

    }

}