<?php


namespace App\Helper;


use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Pagerfanta\Doctrine\Collections\SelectableAdapter;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class Pagination
 * @package App\Helper
 */
class Pagination
{

    const ITEMS_PER_PAGE = 2;

    private array $items;

    private int $total;

    private int $count;


    /**
     * Pagination constructor.
     * @param int $page
     * @param QueryBuilder $qb
     * @throws Exception
     */
    public function __construct(int $page, QueryBuilder $qb) {

        $adapter = new QueryAdapter($qb);
        $pagerfanta = new Pagerfanta($adapter);

        if ($pagerfanta->getNbResults() === 0)
            throw new Exception('No items found into this database', Response::HTTP_NO_CONTENT);

        $pagerfanta->setMaxPerPage($this::ITEMS_PER_PAGE);
        $pagerfanta->setCurrentPage($page);

        $items = [];
        foreach ($pagerfanta->getCurrentPageResults() as $item) {
            $items[] = $item;
        }

        $this->setItems($items);

        $this->setTotal($pagerfanta->getNbResults());;

        $this->setCount(count($items));

    }

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param array $items
     */
    public function setItems(array $items): void
    {
        $this->items = $items;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     */
    public function setTotal(int $total): void
    {
        $this->total = $total;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @param int $count
     */
    public function setCount(int $count): void
    {
        $this->count = $count;
    }


    /**
     * @param int $itemId
     * @param array $itemsId
     * @return integer|null
     */
    public static function getPageNumberForSpecificItem(int $itemId, array $itemsId) : ?int
    {

        $itemPerPage = Pagination::ITEMS_PER_PAGE;

        $page = 0;
        foreach ($itemsId as $item) {
            $page++;
            if ($item['id'] === $itemId)
                return intval(round($page / $itemPerPage, 0));
        }

        return null;
    }

    /**
     * @param array $itemsId
     * @return int|null
     */
    public static function getPageQuantity(array $itemsId) : ?int
    {

        $itemPerPage = Pagination::ITEMS_PER_PAGE;

        $itemQuantity = count($itemsId);

        if ($itemQuantity) {
            return intval(round($itemQuantity / $itemPerPage, 0));
        }

        return null;

    }

}