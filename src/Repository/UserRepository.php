<?php


namespace App\Repository;


use App\Entity\Customer;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class UserRepository
 * @package App\Repository
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }


    public function findAllQueryBuilder($customerId = '')
    {
        if($customerId) {
            $customer = $this->getEntityManager()->getRepository(Customer::class)->find($customerId);
            if ($customer === null){
                throw new HttpException(Response::HTTP_NOT_FOUND, 'No customer found with this id')
;            }
        }
        $qb = $this->createQueryBuilder('user');
        if ($customerId) {
            $qb->andWhere('user.customer = :filter')
                ->setParameter('filter', $customerId);
        }
        return $qb;
    }

    public function findAllIds()
    {
        $qb = $this->createQueryBuilder('u');
        $qb->select('u.id')
            ->orderBy('u.id');

        return $qb->getQuery()->execute();
    }
}