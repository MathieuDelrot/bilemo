<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Hateoas\Configuration\Annotation as Hateoas;


/**
 * Class Product
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 * @Hateoas\Relation(
 *     "collection",
 *     href = @Hateoas\Route(
 *     "api_products_collection_get",
 *     absolute = true
 *      )
 * )
 * @Hateoas\Relation(
 *     "item",
 *     href = @Hateoas\Route(
 *     "api_products_item_get",
 *     parameters = { "id" = "expr(object.getId())" },
 *     absolute = true
 *      )
 * )
 */
class Product
{

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private int $id;

    /**
     * @var string
     * @ORM\Column
     */
    private string $title;

    /**
     * @var string
     *@ORM\Column(type="text")
     */
    private string $description;

    /**
     * @var float
     * currency : euros
     * @ORM\Column(type="float")
     */
    private float $priceWithoutTax;

    /**
     * @var float
     * currency : euros
     * @ORM\Column(type="float")
     */
    private float $priceWithTaxes;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private int $weightInGram;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private int $heightInMillimeter;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private int $widthInMillimeter;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private int $thicknessInMillimeter;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    private float $screenInInch;

    /**
     * @var string
     * @ORM\Column
     */
    private string $cellularTechnology;

    /**
     * @var string
     *@ORM\Column
     */
    private string $operatingSystem;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private int $batteryInMilliAmpereHour;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private int $cameraInMegaPixel;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Product
     */
    public function setId(int $id): Product
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Product
     */
    public function setTitle(string $title): Product
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Product
     */
    public function setDescription(string $description): Product
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return float
     */
    public function getPriceWithoutTax(): float
    {
        return $this->priceWithoutTax;
    }

    /**
     * @param float $priceWithoutTax
     * @return Product
     */
    public function setPriceWithoutTax(float $priceWithoutTax): Product
    {
        $this->priceWithoutTax = $priceWithoutTax;
        return $this;
    }

    /**
     * @return float
     */
    public function getPriceWithTaxes(): float
    {
        return $this->priceWithTaxes;
    }

    /**
     * @param float $priceWithTaxes
     * @return Product
     */
    public function setPriceWithTaxes(float $priceWithTaxes): Product
    {
        $this->priceWithTaxes = $priceWithTaxes;
        return $this;
    }

    /**
     * @return int
     */
    public function getWeightInGram(): int
    {
        return $this->weightInGram;
    }

    /**
     * @param int $weightInGram
     * @return Product
     */
    public function setWeightInGram(int $weightInGram): Product
    {
        $this->weightInGram = $weightInGram;
        return $this;
    }

    /**
     * @return int
     */
    public function getHeightInMillimeter(): int
    {
        return $this->heightInMillimeter;
    }

    /**
     * @param int $heightInMillimeter
     * @return Product
     */
    public function setHeightInMillimeter(int $heightInMillimeter): Product
    {
        $this->heightInMillimeter = $heightInMillimeter;
        return $this;
    }

    /**
     * @return int
     */
    public function getWidthInMillimeter(): int
    {
        return $this->widthInMillimeter;
    }

    /**
     * @param int $widthInMillimeter
     * @return Product
     */
    public function setWidthInMillimeter(int $widthInMillimeter): Product
    {
        $this->widthInMillimeter = $widthInMillimeter;
        return $this;
    }

    /**
     * @return int
     */
    public function getThicknessInMillimeter(): int
    {
        return $this->thicknessInMillimeter;
    }

    /**
     * @param int $thicknessInMillimeter
     * @return Product
     */
    public function setThicknessInMillimeter(int $thicknessInMillimeter): Product
    {
        $this->thicknessInMillimeter = $thicknessInMillimeter;
        return $this;
    }

    /**
     * @return float
     */
    public function getScreenInInch(): float
    {
        return $this->screenInInch;
    }

    /**
     * @param float $screenInInch
     * @return Product
     */
    public function setScreenInInch(float $screenInInch): Product
    {
        $this->screenInInch = $screenInInch;
        return $this;
    }

    /**
     * @return string
     */
    public function getCellularTechnology(): string
    {
        return $this->cellularTechnology;
    }

    /**
     * @param string $cellularTechnology
     * @return Product
     */
    public function setCellularTechnology(string $cellularTechnology): Product
    {
        $this->cellularTechnology = $cellularTechnology;
        return $this;
    }

    /**
     * @return string
     */
    public function getOperatingSystem(): string
    {
        return $this->operatingSystem;
    }

    /**
     * @param string $operatingSystem
     * @return Product
     */
    public function setOperatingSystem(string $operatingSystem): Product
    {
        $this->operatingSystem = $operatingSystem;
        return $this;
    }

    /**
     * @return int
     */
    public function getBatteryInMilliAmpereHour(): int
    {
        return $this->batteryInMilliAmpereHour;
    }

    /**
     * @param int $batteryInMilliAmpereHour
     * @return Product
     */
    public function setBatteryInMilliAmpereHour(int $batteryInMilliAmpereHour): Product
    {
        $this->batteryInMilliAmpereHour = $batteryInMilliAmpereHour;
        return $this;
    }

    /**
     * @return int
     */
    public function getCameraInMegaPixel(): int
    {
        return $this->cameraInMegaPixel;
    }

    /**
     * @param int $cameraInMegaPixel
     * @return Product
     */
    public function setCameraInMegaPixel(int $cameraInMegaPixel): Product
    {
        $this->cameraInMegaPixel = $cameraInMegaPixel;
        return $this;
    }


}