<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;
use Hateoas\Configuration\Annotation as Hateoas;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class User
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @Hateoas\Relation(
 *     "collection",
 *     href = @Hateoas\Route(
 *     "api_users_collection_get",
 *      parameters = { "customerId" = "expr(object.getCustomer().getId())" },
 *      absolute = true
 *      ),
 *     exclusion = @Hateoas\Exclusion(groups={"users"})
 * )
 * @Hateoas\Relation(
 *     "item",
 *     href = @Hateoas\Route(
 *     "api_users_item_get",
 *      parameters = { "id" = "expr(object.getId())" },
 *      absolute = true
 *      ),
 *     exclusion = @Hateoas\Exclusion(groups={"users"})
 * )
 * @Hateoas\Relation(
 *     "delete",
 *     href = @Hateoas\Route(
 *     "api_users_item_delete",
 *     parameters = { "id" = "expr(object.getId())" },
 *     absolute = true
 *      ),
 *     exclusion = @Hateoas\Exclusion(groups={"users"})
 * )
 * @Hateoas\Relation(
 *     "Customer",
 *     embedded = @Hateoas\Embedded("expr(object.getCustomer())"),
 *     exclusion = @Hateoas\Exclusion(groups={"users"})
 * )
 *
 */
class User
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     * @Groups({"users"})
     */
    private int $id;

    /**
     * @var string
     * @ORM\Column
     * @Groups({"users"})
     * @Assert\NotBlank
     */
    private string $firstName;

    /**
     * @var string
     * @ORM\Column
     * @Groups({"users"})
     * @Assert\NotBlank
     */
    private string $lastName;

    /**
     * @var string
     * @ORM\Column
     * @Groups({"users"})
     * @Assert\NotBlank()
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email."
     * )
     */
    private string $email;

    /**
     * @var Customer
     * @ORM\ManyToOne(targetEntity="App\Entity\Customer", inversedBy="users", cascade={"persist"})
     */
    private Customer $customer;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return User
     */
    public function setId(int $id): User
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return User
     */
    public function setFirstName(string $firstName): User
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return User
     */
    public function setLastName(string $lastName): User
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return User
     */
    public function setEmail(string $email): User
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     * @return User
     */
    public function setCustomer(Customer $customer): User
    {
        $this->customer = $customer;
        return $this;
    }

}