<?php

namespace App\DataFixtures;

use App\Entity\Customer;
use App\Entity\Product;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class AppFixtures
 * @package App\DataFixtures
 */
class AppFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private UserPasswordEncoderInterface $userPasswordEncoder;

    /**
     * AppFixtures constructor.
     * @param UserPasswordEncoderInterface $userPasswordEncoder
     */
    public function __construct(UserPasswordEncoderInterface $userPasswordEncoder)
    {
        $this->userPasswordEncoder = $userPasswordEncoder;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $datas = self::getFakeDataModelForFiveUsers();

        $i = 0;
        foreach ($datas as $data) {
            $i++;
            if ($i % 2 !== 0) {
                $customer = new Customer();
                $customer->setCompanyName($data->location->state)
                    ->setEmail($data->email)
                    ->setPassword($this->userPasswordEncoder->encodePassword($customer, 'password'));
                $manager->persist($customer);

            } else {
                $user = new User();
                $user->setLastName($data->name->last)
                    ->setFirstName($data->name->first)
                    ->setCustomer($customer)
                    ->setEmail($data->email);
                $manager->persist($user);
            }
        }

        $product = new Product();
        $product->setBatteryInMilliAmpereHour(6200)
            ->setCameraInMegaPixel(12)
            ->setCellularTechnology("5 G")
            ->setDescription(file_get_contents('http://loripsum.net/api'))
            ->setHeightInMillimeter(74)
            ->setOperatingSystem("Android")
            ->setPriceWithoutTax(300)
            ->setPriceWithTaxes(360)
            ->setScreenInInch(6.78)
            ->setThicknessInMillimeter(8)
            ->setTitle("Hello Phone")
            ->setWeightInGram(220)
            ->setWidthInMillimeter(74);
        $manager->persist($product);

        $product = new Product();
        $product->setBatteryInMilliAmpereHour(6200)
            ->setCameraInMegaPixel(24)
            ->setCellularTechnology("5 G")
            ->setDescription(file_get_contents('http://loripsum.net/api'))
            ->setHeightInMillimeter(72)
            ->setOperatingSystem("Android")
            ->setPriceWithoutTax(300)
            ->setPriceWithTaxes(360)
            ->setScreenInInch(6)
            ->setThicknessInMillimeter(10)
            ->setTitle("Build Phone")
            ->setWeightInGram(220)
            ->setWidthInMillimeter(74);
        $manager->persist($product);

        $manager->flush();

    }

    private static function getFakeDataModelForFiveUsers($i = 0)
    {
        if ($i === 5)
            return [];
        $curl_handle=curl_init();
        curl_setopt($curl_handle, CURLOPT_URL,'https://randomuser.me/api/?results=10');
        curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Bilemo');
        $data = curl_exec($curl_handle);

        if($data === false) {
            print_r("Hello");
            return self::getFakeDataModelForFiveUsers($i+1);
        } else {
            $fakeData = json_decode($data);

            $dt = $fakeData->results;

            curl_close($curl_handle);

            return $dt;
        }
    }
}
