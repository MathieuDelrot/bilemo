# BileMo

BileMo is a Symfony api about mobile phones informations for BileMo Customers.

## Prerequisite

- PHP >= 7.4.
- Mysql >= 10.2
- [Symfony server last version](https://symfony.com/doc/current/setup/symfony_server.html)

- To check compatibility you can launch thsi command into project folder (when project is cloned)

```bash
composer require symfony/requirements-checker
```

## Installation

1 :In your terminal, in the folder you want to install this project launch the following command to download the project from gitlab.

```bash
git clone git@gitlab.com:MathieuDelrot/bilemo.git
```

2 :In your terminal go to the project folder and launch the following command to launch Symfony server

```bash
symfony server:start
```

3 :Start your mysql server with MAMP, WAMP ou Mysql

4 :Install dependencies
```bash
composer install
```

5 :Create database with following command

```bash
php bin/console doctrine:database:create
```

6 :Create schema with following command

```bash
php bin/console doctrine:schema:create
```

7 :Launch datas fixtures

```bash
php bin/console doctrine:fixtures:load
```

## Access to api documentation

8 :Access to api doc by typing http://127.0.0.1:8000/doc into your browser



## Quality code badge from Codacy
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/a57416d4e4da4668863849ea9f40aa41)](https://www.codacy.com/gl/MathieuDelrot/snow-tricks/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=MathieuDelrot/snow-tricks&amp;utm_campaign=Badge_Grade)
